package com.JayPi4c;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

public class VectorTest {

	@Test
	public void toArrayTest() {

		Vector v = new Vector(5);
		double[] data = v.toArray();
		double[] target = { 0.0, 0.0, 0.0, 0.0, 0.0 };
		assertTrue(Arrays.equals(data, target));

	}

	@Test
	public void getTest() {
		Vector v = new Vector(new double[] { 1, 2, 3, 4 });
		assertTrue(v.get(0) == 1);
		assertTrue(v.get(3) == 4);
	}

}
